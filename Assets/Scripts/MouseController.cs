﻿using System;
using UnityEngine;

public class MouseController : IInputController
{
    public Action OnClick { get; set; }
    public Action<Vector2> OnMove { get; set; }

    public Action<Vector2> OnPressDown{ get; set; }

    private bool _isPressed;
    private Vector2 _pressedStartPosition;
    private Vector2 _pressedBeforePosition;

    public void Update()
    {
        if (Input.GetMouseButton(0))
        {
            if (_isPressed)
                OnMove?.Invoke(Input.mousePosition);
        }

        if (Input.GetMouseButtonDown(0))
        {
            _isPressed = true;
            OnPressDown?.Invoke(Input.mousePosition);
        }


        if (Input.GetMouseButtonUp(0))
        {
            OnClick?.Invoke();
            _pressedBeforePosition = default;
            _isPressed = false;
        }
    }
}