﻿using System.Linq;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private Camera mainCamera;
    [SerializeField] private Transform mainCameraTransform;
    private IInputController _inputController;
    private Vector3 _worldPressDownPosition;
    private Vector3 _cameraStartPosition;

    public void Start()
    {
        _inputController = new MouseController();
        _inputController.OnMove += OnCameraMove;
        _inputController.OnPressDown += OnStartMove;
    }

    private void Update()
    {
        _inputController.Update();
    }

    private void OnStartMove(Vector2 screenPosition)
    {
        var raycastHits = Physics.RaycastAll(mainCamera.ScreenPointToRay(screenPosition), 1000f,
            1 << LayerMask.NameToLayer("Ground"));
        if (raycastHits.Length == 0) return;
        _worldPressDownPosition = raycastHits[0].point;
        _cameraStartPosition = mainCameraTransform.position;
    }

    private void OnCameraMove(Vector2 screenPosition)
    {
        var raycastHits = Physics.RaycastAll(mainCamera.ScreenPointToRay(screenPosition), 1000f,
            1 << LayerMask.NameToLayer("Ground"));
        if (raycastHits.Length == 0) return;
        var hit = raycastHits[0];
        var hitPosition = hit.point;
     
        var deltaMove =  _worldPressDownPosition - hitPosition;
        var newCameraPosition = _cameraStartPosition + deltaMove*0.5f;
        newCameraPosition.y = mainCameraTransform.position.y;
        mainCameraTransform.position = newCameraPosition;
    }
}