﻿using System;
using UnityEngine;

public interface IInputController
{
    Action OnClick { get; set; }

    // Vector2 - delta move
    Action<Vector2> OnMove { get; set; }
    Action<Vector2> OnPressDown { get; set; }
    void Update();
}